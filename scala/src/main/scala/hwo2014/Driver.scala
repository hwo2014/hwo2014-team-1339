package hwo2014

import scala.collection.mutable.HashMap

private case class Data(
  var length: Double = 0.0,
  var changeTo: Int = 0,
  var radius: Double = Double.PositiveInfinity,
  var speed: Double = 0.0,
  var nextSpeed: Double = 0.0,
  var turbo: Boolean = false
)

private case class PositionTracking(
  var lastLength: Double = 0.0,
  var currentPos: Double = 0.0,
  var speed: Double = 0.0,
  var currentPiece: Int = 0,
  var distance: Double = 0.0,
  var isSwitching: Boolean = false,
  var lane: Int = 0,
  var onTrack: Boolean = true
)

class Driver(val race: Race, val colour: String) {
  private val laps = race.raceSession.laps.getOrElse(1)
  private val pieces = race.track.pieces.size
  private val lanes = race.track.lanes.size
  private var laneLength: Array[Double] = null
  private var laneMap: Map[Int,Int] = null
  private val data = Array.fill[Data](laps,pieces,lanes)(new Data)
  private var f: Double = 0.0
  private var am: Double = 0.0
  private var theLap: Int = 0
  private var turboAvail: Boolean = false
  private var turboFactor: Double = 1.0
  private var usedTurboFactor: Double = 1.0
  private var speeds: List[Double] = Nil
  private var simulationReady: Boolean = false
  private var lastAskedForChangeIn: Int = -1

  private var angle: Double = 0.0
  private var Vangle: Double = 0.0
  private var Aangle: Double = 0.0
  private var force: Double = 0.0
  private var angles: List[Double] = Nil
  private var speed1: Double =  0.0

  private var bestE: Double = 1.2*981
  private var bestF: Double = -6.0
  private var bestG: Double = -0.075
  private var fixedFG: Boolean = false
  private var fixedE: Boolean = false
  private var tryCorrect: Boolean = false
  private var tryCorrectSlow: Boolean = false
  private var breaks: Int = 0

  private var carsPositions = new HashMap[String, PositionTracking]

  private var risky: Boolean = false

  getLaneLengths()
  measurePieces()
  optimizeLanes()
  if (laps > 1) findTurbo()
  findLongest()
  fillCarsPositionsWithCars()

  def lapFinished(): Unit = {
    println("Lap finished")
    if (theLap < laps-1) {
      theLap += 1
      if (!risky) {
        bestE += 160
      } else {
        bestE += 30
      }
      findMaxSpeed()
      println(s"Increased E $bestE")
      risky = false
    }
  }

  def gotTurbo(factor: Double): Unit = {
    if (carsPositions.isDefinedAt(colour) && carsPositions(colour).onTrack) {
      turboAvail = true
      turboFactor = factor
    }
  }

  def usedTurbo(): Unit = {
    turboAvail = false
  }

  def turboActivated(): Unit = {
    am *= turboFactor
    usedTurboFactor = turboFactor
  }

  def turboFinished(): Unit = {
    am /= usedTurboFactor
  }

  def readyToDecideOnLaneChange(p: PiecePosition): Boolean = {
    val piece = p.pieceIndex
    if (lastAskedForChangeIn != piece) {
      val lane = laneMap(p.lane.endLaneIndex)
      val now = data(theLap)(piece)(lane)
      val decision = (p.inPieceDistance + am/f/30 >= now.length)
      if (decision) lastAskedForChangeIn = piece
      decision
    } else {
      false
    }
  }

  def getLaneChange(p: PiecePosition): Int = {
    val piece = (p.pieceIndex+1)%pieces
    val lane = laneMap(p.lane.endLaneIndex)
    if (race.track.pieces(piece).withSwitch) {
      // Choosing pieces:
      var currentPiece = piece
      var length = 1
      while (currentPiece!=piece-1 && length < 2) {
        currentPiece += 1
        if (race.track.pieces(currentPiece % pieces).withSwitch) {
          length += 1
        }
      }

      // Finding (for each lane) a car with minimal distance:
      val distances = Array.fill[Double](lanes)(Double.PositiveInfinity)
      for( (k, pt) <- carsPositions; if (k!=colour);
        if ( (pt.currentPiece >= piece && pt.currentPiece < currentPiece)
          || (currentPiece >= pieces && pt.currentPiece < currentPiece%pieces) )) {

        distances(pt.lane) = math.min(distances(pt.lane), pt.distance)
      }

      // Choosing best neighbouring lane:
      var maximalDistance = distances(lane)
      var best: List[Int] = List(0)
      if (lane > 0) {
        if (distances(lane-1) > maximalDistance) {
          maximalDistance = distances(lane-1)
          best = List(-1)
        } else if (distances(lane-1)==maximalDistance) {
          best = -1::best
        }
      }
      if (lane < lanes-1) {
        if (distances(lane+1) > maximalDistance) {
         maximalDistance = distances(lane+1)
         best = List(+1)
        } else if (distances(lane+1)==maximalDistance) {
           best = 1::best
        }
      }

      val change = data(theLap)(piece)(lane).changeTo
      if (best contains change) {
        change
      } else {
        best((new scala.util.Random).nextInt(best.size))
      }
    } else {
      0
    }
  }

  def getTurbo(p: PiecePosition): Boolean = {
    val piece = p.pieceIndex
    val lane = laneMap(p.lane.startLaneIndex)
    turboAvail && data(theLap)(piece)(lane).turbo
  }

  def reportFinish(color: String) {
    carsPositions remove color
  }

  def reportCrash(color: String) {
    if (color == colour) {
      println("Got crash")
      angle = 0
      Vangle = 0
      Aangle = 0
      if (breaks > 0) {
        bestE = bestE+breaks*60*0.75
        println(s"New E (regain): $bestE (old: ${823.5*f})")
        findMaxSpeed()
        breaks = 0
      }
    }
    if (carsPositions.isDefinedAt(color)) {
      carsPositions(color).onTrack = false
    }
  }

  def reportSpawn(color: String) {
    if (color == colour) {
      println("Got spawn")
    }
    if (carsPositions.isDefinedAt(color)) {
      carsPositions(color).onTrack = true
    }
  }

  def carPositionToAlpha(lcp: List[CarPosition]): Double = {
    updateCarsPositions(lcp)
    carsPositions.get(colour) match {
      case None => 0.0
      case Some(pt) =>
      if (!simulationReady) {
        speeds = (pt.speed) :: speeds
        if (speeds.size == 4) {
          val sp = speeds.reverse
          val v1 = sp(1)
          val v2 = sp(2)
          val v3 = sp(3)
          val a2 = (v2-v1)*60
          val a3 = (v3-v2)*60
          f = (a2-a3)/(v2-v1)
          am = a3+f*v2
          println(s"Estimated parameters: am=$am, f=$f")
          bestE = bestE/1.2*f
          findMaxSpeed()
          simulationReady = true
        }
        1.0
      } else {
        if (pt.speed==0) {
          1.0
        } else {
          val speed = pt.speed
          val laneData = data(theLap)(pt.currentPiece)(pt.lane)
          // going at speed, want to laneData.speed
          if ((speed - laneData.nextSpeed)/f+pt.currentPos >= laneData.length) {
            //time to break
            0.0
          } else {
            val a = f/am*(laneData.speed*math.exp(f/60)-speed)/(math.exp(f/60)-1)
            math.max(0, math.min(1, a))
          }
        }
      }
    }
  }

  private def updateCarsPositions(lcp: List[CarPosition]): Unit = {
    for(cp <- lcp) {
      carsPositions.get(cp.id.color) match {
        case None => ()
        case Some(pt) => {
          val pp = cp.piecePosition

          val lastPos = pt.currentPos
          pt.currentPos = pp.inPieceDistance
          val diff = pt.currentPos - lastPos + (if (pt.currentPos-lastPos < 0) pt.lastLength else 0)
          pt.distance += diff
          val oldSpeed = pt.speed
          pt.speed = diff*60
          pt.lastLength = data(math.max(0, math.min(pp.lap, laps-1)))(pp.pieceIndex)(laneMap(pp.lane.endLaneIndex)).length

          val lastPiece = pt.currentPiece
          pt.currentPiece = pp.pieceIndex
          pt.isSwitching = pp.lane.startLaneIndex != pp.lane.endLaneIndex
          pt.lane = laneMap(pp.lane.endLaneIndex)

          if (cp.id.color == colour) {
            val r = data(theLap)(pt.currentPiece)(pt.lane).radius
            if (math.abs(angle) > 55) {
              risky = true
            }
            val angleNow = cp.angle
            val VangleNow = (angleNow-angle)*60
            if (!fixedFG && angle!=0 && angles.size < 4 && r==Double.PositiveInfinity) {
              angles = (angleNow) :: angles
              if (angles.size==2){
                speed1 = pt.speed
              }
              if (angles.size==4){
                angles = angles.reverse
                val v1 = 60*(angles(1) - angles(0))
                val v2 = 60*(angles(2) - angles(1))
                val v3 = 60*(angles(3) - angles(2))
                val a2 = 60*(v2-v1)
                val a3 = 60*(v3-v2)

                val detA = v2*angles(1)*speed1 - v1*angles(2)*oldSpeed
                val detF = a3*angles(1)*speed1 - a2*angles(2)*oldSpeed
                val F = detF/detA
                if (math.abs(F-bestF) > 1){
                  bestF = F
                  println(s"Parametr F differs from -6: $bestF")
                }
                val detG = v2*a2 - v1*a3
                val G = detG/detA
                if (math.abs(G-bestG) > 0.01){
                  bestG = G
                  println(s"Parametr G differs from -0.075: $bestG")
                }
                fixedFG = true
                println("Fixed F and G")
              }
            } else {
              angles = Nil
            }
            Aangle = (VangleNow-Vangle)*60
            force = Aangle - (bestF*Vangle+bestG*angle*oldSpeed)
            angle = angleNow
            Vangle = VangleNow
            val Cf = pt.speed*pt.speed/r
            //println(s"   debug >>> Cf: $Cf, angle: $angle, Vangle: $Vangle, Aangle: $Aangle, force: $force")
            if ( math.abs(force)>100 && (
              (math.abs(angle)>20 && math.abs(angle)+math.abs(Vangle)>180 && angle*Vangle>0 && Vangle*Aangle>0) ||
              (math.abs(angle)>30 && math.abs(angle)+math.abs(Vangle)>180 && angle*Vangle>0 && Vangle*Aangle<0) ||
              (math.abs(angle)>45 && math.abs(angle)+math.abs(Vangle)>145 && angle*Vangle>0 && Vangle*Aangle<0) ||
              (math.abs(force)>2014) ||
              (math.abs(angle)>55)
            )) {
              bestE = math.max(60, bestE-60)
              println(s"New E (break): $bestE (old: ${823.5*f})")
              breaks = breaks + 1
              findMaxSpeed()
            }
            if (math.abs(force)<100) {
              if (breaks > 0) {
                bestE = bestE+math.max(math.min(breaks*60*0.95, breaks*60-30), breaks*60-90)
                println(s"New E (regain): $bestE (old: ${823.5*f})")
                findMaxSpeed()
                breaks = 0
              }
            }
            if (lastPiece != pt.currentPiece) {
              tryCorrect = false
              tryCorrectSlow = false
            }
            if (!tryCorrectSlow && math.abs(force)>1 && Cf > 0) {
              tryCorrectSlow = true
            } else {
              if (tryCorrectSlow && math.abs(force)>1 && Cf > 0) {
                if (!fixedE && Cf < bestE) {
                  fixedE = true
                  bestE = math.max(60, math.max(bestE-60, (Cf+bestE)/2))
                  println(s"New E (down): $bestE (old: ${823.5*f})")
                  findMaxSpeed()
                }
              }
              tryCorrectSlow = false
            }
            if (!tryCorrect && (math.abs(angle) < 1e-2) && (math.abs(Vangle) < 1e-2) && (math.abs(Aangle) < 1e-2) && Cf > bestE) {
              tryCorrect = true
              fixedE = false
            } else {
              if (tryCorrect && (math.abs(angle) < 1e-2) && (math.abs(Vangle) < 1e-2) && (math.abs(Aangle) < 1e-2) && Cf > bestE) {
                bestE = math.min(bestE+60, (Cf+bestE)/2)
                println(s"New E (up): $bestE (old: ${823.5*f})")
                findMaxSpeed()
              }
              tryCorrect = false
            }
          }
        }
      }
    }
  }

  private def getLaneLengths(): Unit = {
    val temp = race.track.lanes.sortBy(_.distanceFromCenter)
    laneLength = temp.map(_.distanceFromCenter).toArray
    laneMap = temp.map(_.index).zipWithIndex.toMap
  }

  private def measurePieces(): Unit = {
    def measure(pieces: List[Piece], current: Int): Unit = pieces match {
      case Piece(Some(length), switch, _, _)::tail => {
        for (lap <- 0 until laps; lane <- 0 until lanes) {
          val dataLCL = data(lap)(current)(lane)
          dataLCL.length = length
          dataLCL.changeTo = if (switch.isDefined && switch.get) 1 else 0
        }
        measure(tail, current+1)
      }
      case Piece(_, switch, Some(radius), Some(angle))::tail => {
        for (lane <- 0 until lanes) {
          val r = radius + laneLength(lane)*(if (angle>0) -1 else 1)
          val phi = math.toRadians(angle)
          val length = math.abs(r*phi)
          for (lap <- 0 until laps) {
            val dataLCL = data(lap)(current)(lane)
            dataLCL.length = length
            dataLCL.changeTo = if (switch.isDefined && switch.get) 1 else 0
            dataLCL.radius = r
          }
        }
        measure(tail, current+1)
      }
      case _ => ()
    }
    measure(race.track.pieces, 0)
  }

  private def optimizeLanes(): Unit = {
    val roadSoFar = Array.fill[Double](lanes)(0.0)
    val tempRoad = Array.fill[Double](lanes)(0.0)
    for (lap <- laps-1 to 0 by -1; piece <- pieces-1 to 0 by -1) {
      if (data(lap)(piece)(0).changeTo == 1) {
        for (lane <- 0 until lanes) {
          var best = roadSoFar(lane)
          var id = 0
          if (lane>0) {
            val now = roadSoFar(lane-1)
            if (now < best) {
              best = now
              id = -1
            }
          }
          if (lane < lanes-1) {
            val now = roadSoFar(lane+1)
            if (now < best) {
              best = now
              id = 1
            }
          }
          data(lap)(piece)(lane).changeTo = id
          tempRoad(lane) = best+data(lap)(piece)(lane).length
        }
        for (lane <- 0 until lanes) {
          roadSoFar(lane) = tempRoad(lane)
        }
      } else {
        for (lane <- 0 until lanes) {
          roadSoFar(lane) += data(lap)(piece)(lane).length
        }
      }
    }
  }

  private def findMaxSpeed(): Unit = {
    var lookFrom = 0.0
    var last = Double.PositiveInfinity
    var now = data(0)(0)
    while (lookFrom != last) {
      lookFrom = last
      findMaxSpeedFrom(lookFrom)
      last = 0.0
      for (lane <- 0 until lanes) {
        if (last < now(lane).speed) last = now(lane).speed
      }
      if (laps > 1) return
    }
  }

  private def findMaxSpeedFrom(lookFrom: Double): Unit = {
    var now = data(laps-1)(pieces-1)
    for (lane <- 0 until lanes) {
      var length = now(lane).length
      var speed = math.sqrt(bestE*now(lane).radius)*1.25
      if (lookFrom < speed) {
        val break = (speed-lookFrom)/f
        if (break>length) {
          speed = lookFrom + f*length
        }
      }
      now(lane).speed = speed
      now(lane).nextSpeed = lookFrom
    }
    for (lap <- laps-1 to 0 by -1; piece <- pieces-1 to 0 by -1; if (piece != pieces-1 || lap != laps-1)) {
      now = data(lap)(piece)
      var next = if (piece==pieces-1) data(lap+1)(0) else data(lap)(piece+1)
      for (lane <- 0 until lanes) {
        var length = now(lane).length
        var speed = math.sqrt(bestE*now(lane).radius)*1.25
        var target = next(lane).speed
        if (lane < lanes-1) {
          target = math.min(target, next(lane+1).speed)
        }
        if (lane > 0) {
          target = math.min(target, next(lane-1).speed)
        }
        if (target < speed) {
          val break = (speed-target)/f
          if (break>length) {
            speed = target + f*length
          }
        }
        now(lane).speed = speed
        now(lane).nextSpeed = target
      }
    }
  }

  private def findTurbo(): Unit = {
    for (lap <- laps-1 to 0 by -1; piece <- pieces-1 to 0 by -1) {
      val now = data(lap)(piece)
      if (now(0).radius == Double.PositiveInfinity) {
        for (lane <- 0 until lanes) {
          now(lane).turbo = true
        }
      } else { return }
    }
  }

  private def findLongest(): Unit = {
    var longest = 0.0
    var idL = -1
    var now = 0.0
    var idN = -1
    for (lap <- 0 to 1; piece <- 0 until pieces) {
      val P = data(lap%laps)(piece)(0)
      if (P.radius == Double.PositiveInfinity) {
        now += P.length
        if (lap==0 && idN == -1) idN = piece
      } else {
        if (now > longest) {
          longest = now
          idL = idN
        }
        now = 0.0
        idN = -1
      }
    }
    for (lap <- 0 until math.max(1, laps-1); lane <- 0 until lanes) {
      data(lap)(idL)(lane).turbo = true
    }
  }

  private def fillCarsPositionsWithCars(): Unit = {
    for(car <- race.cars) {
      carsPositions += car.id.color -> new PositionTracking
    }
  }
}
