package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec

object SpecBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new SpecBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class SpecBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  send(MsgOutWrapper("join", Join(botName, botKey)))

  var driver: Driver = null
  var myCar: String = ""
  var turboNext: Boolean = false
  var alpha: Double = 1.0
  var change: Int = 0
  var debug: Boolean = false

  play

  @tailrec private def play {
    val line = reader.readLine()
    if (line != null) {
      Serialization.read[MsgInWrapper](line) match {
        case MsgInWrapper("yourCar", data,  _) => {
          myCar = data.extract[YourCar].color
        }
        case MsgInWrapper("gameInit", data, _) => {
          driver = new Driver(data.extract[GameInit].race, myCar)
        }
        case MsgInWrapper("lapFinished", data, _) => {
          if (data.extract[LapFinished].car.color == myCar) {
            driver.lapFinished()
          }
        }
        case MsgInWrapper("turboAvailable", data, _) => {
          driver.gotTurbo(data.extract[Turbo].turboFactor)
        }
        case MsgInWrapper("turboStart", data, _) => {
          if (data.extract[TurboStartEnd].color == myCar) {
            driver.turboActivated()
          }
        }
        case MsgInWrapper("turboEnd", data, _) => {
          if (data.extract[TurboStartEnd].color == myCar) {
            driver.turboFinished()
          }
        }
        case MsgInWrapper("gameStart",data, gameTick) => {
          send(MsgOutWrapper("throttle", 1.0, gameTick))
        }
        case MsgInWrapper("carPositions", data, gameTick) => {
          val cps = data.extract[List[CarPosition]]
          val myPosition = cps.filter{ cp => cp.id.color==myCar }.head
          val p = myPosition.piecePosition
          alpha = driver.carPositionToAlpha(cps)
          if (gameTick.isDefined) {
            var msg = MsgOutWrapper("throttle", alpha, gameTick)
            if (turboNext) {
              msg = MsgOutWrapper("turbo", "Aaaargh!!!")
              driver.usedTurbo()
            }
            if (driver.readyToDecideOnLaneChange(p)) {
              change = driver.getLaneChange(p)
              if (change == 1) {
                msg = MsgOutWrapper("switchLane", "Right")
              }
              if (change == -1) {
                msg = MsgOutWrapper("switchLane", "Left")
              }
            }
            send(msg)
          }
          turboNext = driver.getTurbo(p)
        }
        case MsgInWrapper("crash", data, _) => {
          debug = false
          driver.reportCrash(data.extract[Crash].color)
        }
        case MsgInWrapper("spawn", data, _) => {
          driver.reportSpawn(data.extract[Spawn].color)
        }
        case MsgInWrapper("finish", data, _) => {
          driver.reportFinish(data.extract[Finish].color)
        }
        case MsgInWrapper("dnf", data, _) => {
          driver.reportFinish(data.extract[Disqualification].car.color)
        }
        case MsgInWrapper(msgType, _, _) => {
          if (!debug) println("Received: " + msgType)
        }
      }
      if (debug) println(line)
      play
    }
  }

  def send(msg: MsgOutWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }
}

case class MsgInWrapper(msgType: String, data: JValue, gameTick: Option[Int] = None)

case class MsgOutWrapper(msgType: String, data: JValue, gameTick: Option[Int])
object MsgOutWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any, gameTick: Option[Int] = None): MsgOutWrapper = {
    MsgOutWrapper(msgType, Extraction.decompose(data), gameTick)
  }
}
