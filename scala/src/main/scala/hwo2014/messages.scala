package hwo2014

import org.json4s._
import org.json4s.native.JsonMethods._

trait Message

trait IncomingMessage

trait OutgoingMessage

case class Join(name: String, key: String) extends OutgoingMessage
case class JoinRace(botId: Join, trackName: String, carCount: Int) extends OutgoingMessage

case class YourCar(name: String, color: String) extends IncomingMessage

case class Piece(length: Option[Double], switch: Option[Boolean], radius: Option[Double], angle: Option[Double]){
	def withSwitch(): Boolean = !switch.isEmpty && switch.get
}
case class Lane(distanceFromCenter: Double, index: Int)
case class Position(x: Double, y: Double)
case class StartingPoint(position: Position, angle: Double)
case class Track(id: String, name: String, pieces: List[Piece], lanes: List[Lane], startingPoint: StartingPoint)

case class CarId(name: String, color: String)
case class Dimensions(length: Double, width: Double, guideFlagPosition: Double)
case class Car(id: CarId, dimensions: Dimensions)

case class RaceSession(durationMs: Option[Int], laps: Option[Int], maxLapTimeMs: Option[Double], quickRace: Option[Boolean])

case class Race(track: Track, cars: List[Car], raceSession: RaceSession)
case class GameInit(race: Race) extends IncomingMessage

case class GameStart() extends IncomingMessage

case class LaneSwitch(startLaneIndex: Int, endLaneIndex: Int)
case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: LaneSwitch, lap: Int)
case class CarPosition(id: CarId, angle: Double, piecePosition: PiecePosition)

case class CarPositions(carPositions: List[CarPosition]) extends IncomingMessage

case class Throttle(throttle: Double) extends OutgoingMessage

case class Result(laps: Option[Int], ticks: Option[Int], millis: Option[Int])
case class CarResult(car: CarId, result: Result)
case class BestLapResult(lap: Option[Int], ticks: Option[Int], millis: Option[Int])
case class CarBestLapResult(car: CarId, result: BestLapResult)

case class GameEnd(results: List[CarResult], bestLaps: List[CarBestLapResult])

case class TournamentEnd() extends IncomingMessage

case class Crash(name: String, color: String)

case class Spawn(name: String, color: String)

case class LapTime(lap: Int, ticks: Int, millis: Int)
case class RaceTime(laps: Int, ticks: Int, millis: Int)
case class Ranking(overall: Int, fastestLap: Int)
case class LapFinished(car: CarId, lapTime: LapTime, raceTime: RaceTime, ranking: Ranking) extends IncomingMessage

case class Disqualified(car: CarId, reason: String) extends IncomingMessage

case class Finish(name: String, color: String) extends IncomingMessage

case class SwitchLane(direction: String) extends OutgoingMessage

case class Turbo(turboDurationMilliseconds: Double, turboDurationTicks: Int, turboFactor: Double) extends IncomingMessage
case class TurboStartEnd(name: String, color: String) extends IncomingMessage
case class Disqualification(car: CarId, reason: String) extends IncomingMessage
